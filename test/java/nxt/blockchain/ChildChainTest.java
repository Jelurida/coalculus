/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2021 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of this software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package nxt.blockchain;

import nxt.BlockchainTest;
import nxt.Constants;
import nxt.http.APITag;
import nxt.util.Convert;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.singleton;
import static nxt.blockchain.chaincontrol.PermissionTestUtil.isChildChainPolicy;
import static org.junit.Assert.assertEquals;

public class ChildChainTest extends BlockchainTest {

    public static final List<ChildChain> GST_CHAINS = Arrays.asList(
            ChildChain.GUSD,
            ChildChain.GEUR,
            ChildChain.GGBP,
            ChildChain.GRMB);

    public static final List<ChildChain> DASH_CHAINS = Arrays.asList(
            ChildChain.DUSD,
            ChildChain.DEUR,
            ChildChain.DCAD,
            ChildChain.DRMB,
            ChildChain.DSGD);

    private static final List<Long> TESTNET_OWNERS_1B = Stream.of(
            "COAL-KDL9-E6WB-TK9M-3CZ5P",
            "COAL-CKXT-4PML-RG76-EHUAQ",
            "COAL-D985-99DV-4YXJ-GWMRN",
            "COAL-XQKC-X6PS-MCTP-83RVN",
            "COAL-2B58-NW68-MV66-HA2QE",

            "COAL-9D4K-YSJG-297Z-442XR",
            "COAL-6N9A-Z2QL-8ALB-DYM7X",
            "COAL-HBEE-EL6G-JEG9-E9S84",
            "COAL-Q2DF-7UKS-TZ9S-5QY2W").map(Convert::parseAccountId).collect(Collectors.toList());
    private static final List<Long> COALCULUS_ACCOUNT_5 = Collections.singletonList(Convert.parseAccountId("COAL-Q4EC-EBMS-7N2J-6ML72"));

    private static final List<Long> NEW_COAL_ACCOUNTS_GST_10K = Stream.of(
            "COAL-3SDC-53GQ-2KY5-G4CCV",
            "COAL-LT4Y-DXPX-WKV3-7X6EV").map(Convert::parseAccountId).collect(Collectors.toList());

    private static final List<Long> NEW_COAL_ACCOUNTS_DASH_10K = Stream.of(
            "COAL-4BJ7-CXJ7-J827-7JTYM",
            "COAL-2YX6-4S5F-PM8B-H6AGR").map(Convert::parseAccountId).collect(Collectors.toList());

    private static final List<Long> NEW_COAL_ACCOUNTS_ASIAN_10K = Stream.of(
            "COAL-UXYH-38UB-W8PH-68LY6",
            "COAL-KESL-6TLU-L93S-C7S7D").map(Convert::parseAccountId).collect(Collectors.toList());
    @Test
    public void testGetDisabledAPITagsPolicyChildChain() {
        ChildChain chain = ChildChain.IGNIS;
        Assume.assumeTrue(isChildChainPolicy(chain));
        assertEquals(EnumSet.of(APITag.AE, APITag.VS, APITag.SHUFFLING, APITag.MS, APITag.ALIASES), chain.getDisabledAPITags());
    }

    @Test
    public void testGetDisabledAPITagsPolicyNone() {
        ChildChain chain = ChildChain.IGNIS;
        Assume.assumeFalse(isChildChainPolicy(chain));
        assertEquals(singleton(APITag.CHILD_CHAIN_CONTROL), chain.getDisabledAPITags());
    }

    @Test
    public void testEnablingChainsGST() {
        List<Long> allAccounts = Stream.of(TESTNET_OWNERS_1B.stream(), COALCULUS_ACCOUNT_5.stream(), NEW_COAL_ACCOUNTS_GST_10K.stream())
                .flatMap(s -> s).collect(Collectors.toList());

        GST_CHAINS.forEach(chain -> allAccounts.forEach(a ->
                Assert.assertEquals(0, getBalance(chain, a))));

        generateBlocks(Constants.UPGRADE_TO_2_3_4_BLOCK + 1);

        GST_CHAINS.forEach(chain -> {
            TESTNET_OWNERS_1B.forEach(acc ->
                    Assert.assertEquals(1_000_000_000 * chain.ONE_COIN, getBalance(chain, acc)));

            Assert.assertEquals(999_980_000 * chain.ONE_COIN, getBalance(chain, COALCULUS_ACCOUNT_5.get(0)));

            NEW_COAL_ACCOUNTS_GST_10K.forEach(acc ->
                    Assert.assertEquals(10_000 * chain.ONE_COIN, getBalance(chain, acc)));
        });
    }

    @Test
    public void testEnablingChainsDASH() {
        List<Long> allAccounts = Stream.of(TESTNET_OWNERS_1B.stream(), COALCULUS_ACCOUNT_5.stream(), NEW_COAL_ACCOUNTS_DASH_10K.stream())
                .flatMap(s -> s).collect(Collectors.toList());

        DASH_CHAINS.forEach(chain -> allAccounts.forEach(a ->
                Assert.assertEquals(0, getBalance(chain, a))));

        generateBlocks(Constants.UPGRADE_TO_2_3_4_BLOCK + 1);

        DASH_CHAINS.forEach(chain -> {
            TESTNET_OWNERS_1B.forEach(acc ->
                    Assert.assertEquals(1_000_000_000 * chain.ONE_COIN, getBalance(chain, acc)));

            Assert.assertEquals(999_980_000 * chain.ONE_COIN, getBalance(chain, COALCULUS_ACCOUNT_5.get(0)));

            NEW_COAL_ACCOUNTS_DASH_10K.forEach(acc ->
                    Assert.assertEquals(10_000 * chain.ONE_COIN, getBalance(chain, acc)));
        });
    }

    @Test
    public void testEnablingChainASIAN() {
        List<Long> allAccounts = Stream.of(TESTNET_OWNERS_1B.stream(), COALCULUS_ACCOUNT_5.stream(), NEW_COAL_ACCOUNTS_ASIAN_10K.stream())
                .flatMap(s -> s).collect(Collectors.toList());

        generateBlocks(Constants.UPGRADE_TO_2_3_4_BLOCK);

        ChildChain chain = ChildChain.ASIAN;
        allAccounts.forEach(a ->
                Assert.assertEquals(0, getBalance(chain, a)));

        generateBlocks(Constants.ASIAN_BLOCK + 1);

        TESTNET_OWNERS_1B.forEach(acc ->
                Assert.assertEquals(100_000_000_000L * chain.ONE_COIN, getBalance(chain, acc)));

        Assert.assertEquals(99_999_980_000L * chain.ONE_COIN, getBalance(chain, COALCULUS_ACCOUNT_5.get(0)));

        NEW_COAL_ACCOUNTS_ASIAN_10K.forEach(acc ->
                Assert.assertEquals(10_000 * chain.ONE_COIN, getBalance(chain, acc)));
    }

    private long getBalance(ChildChain chain, Long acc) {
        return chain.getBalanceHome().getBalance(acc).getBalance();
    }
}
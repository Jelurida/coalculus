/*
 * Copyright © 2016-2021 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of this software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package nxt.http;

import org.junit.Assert;
import org.junit.Test;

public class APIHandlerParametersTest {
    @Test
    public void testCanHaveRecipient() {
        Assert.assertFalse(APIEnum.APPROVE_TRANSACTION.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.BROADCAST_TRANSACTION.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.CALCULATE_FULL_HASH.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.CANCEL_ASK_ORDER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.CANCEL_BID_ORDER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.CAST_VOTE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.CREATE_POLL.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.CURRENCY_BUY.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.CURRENCY_SELL.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.CURRENCY_RESERVE_INCREASE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.CURRENCY_RESERVE_CLAIM.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.CURRENCY_MINT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DECRYPT_FROM.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DELETE_ASSET_SHARES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DGS_LISTING.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DGS_DELISTING.getHandler().canHaveRecipient());
        Assert.assertTrue(APIEnum.DGS_DELIVERY.getHandler().canHaveRecipient());
        Assert.assertTrue(APIEnum.DGS_FEEDBACK.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DGS_PRICE_CHANGE.getHandler().canHaveRecipient());
        Assert.assertTrue(APIEnum.DGS_PURCHASE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DGS_QUANTITY_CHANGE.getHandler().canHaveRecipient());
        Assert.assertTrue(APIEnum.DGS_REFUND.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DECODE_TOKEN.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DECODE_FILE_TOKEN.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DECODE_Q_R_CODE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.ENCODE_Q_R_CODE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.ENCRYPT_TO.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.EVENT_REGISTER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.EVENT_WAIT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GENERATE_TOKEN.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GENERATE_FILE_TOKEN.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_BLOCK_COUNT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_BLOCK_IDS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_BLOCKS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_ID.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_LEDGER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_LEDGER_ENTRY.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_VOTER_PHASED_TRANSACTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_LINKED_PHASED_TRANSACTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_POLLS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_PHASED_TRANSACTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_PHASED_TRANSACTION_COUNT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_PUBLIC_KEY.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_LESSORS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_ASSETS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_CURRENCIES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_CURRENCY_COUNT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_ASSET_COUNT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_PROPERTIES.getHandler().canHaveRecipient());
        Assert.assertTrue(APIEnum.SELL_ALIAS.getHandler().canHaveRecipient());
        Assert.assertTrue(APIEnum.BUY_ALIAS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ALIAS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ALIAS_COUNT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ALIASES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ALIASES_LIKE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ALL_ASSETS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ALL_CURRENCIES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ASSET.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ASSETS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ASSET_IDS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ASSETS_BY_ISSUER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ASSET_ACCOUNTS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ASSET_ACCOUNT_COUNT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ASSET_PHASED_TRANSACTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_BALANCE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_BLOCK.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_BLOCK_ID.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_BLOCKS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_BLOCKCHAIN_STATUS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_BLOCKCHAIN_TRANSACTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_REFERENCING_TRANSACTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_CONSTANTS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_CURRENCY.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_CURRENCIES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_CURRENCY_FOUNDERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_CURRENCY_IDS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_CURRENCIES_BY_ISSUER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_CURRENCY_ACCOUNTS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_CURRENCY_ACCOUNT_COUNT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_CURRENCY_PHASED_TRANSACTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_DGS_GOODS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_DGS_GOODS_COUNT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_DGS_GOOD.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_DGS_GOODS_PURCHASES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_DGS_GOODS_PURCHASE_COUNT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_DGS_PURCHASES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_DGS_PURCHASE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_DGS_PURCHASE_COUNT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_DGS_PENDING_PURCHASES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_DGS_EXPIRED_PURCHASES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_DGS_TAGS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_DGS_TAG_COUNT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_DGS_TAGS_LIKE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_GUARANTEED_BALANCE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_E_C_BLOCK.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_PLUGINS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_MY_INFO.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_PEER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_PEERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_PHASING_POLL.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_PHASING_POLL_VOTES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_PHASING_POLL_VOTE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_POLL.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_POLL_RESULT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_POLL_VOTES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_POLL_VOTE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_STATE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_TIME.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_TRADES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_LAST_TRADES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EXCHANGES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EXCHANGES_BY_EXCHANGE_REQUEST.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EXCHANGES_BY_OFFER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_LAST_EXCHANGES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ALL_TRADES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ALL_EXCHANGES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ASSET_TRANSFERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ASSET_HISTORY.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EXPECTED_ASSET_TRANSFERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EXPECTED_ASSET_DELETES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_CURRENCY_TRANSFERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EXPECTED_CURRENCY_TRANSFERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_TRANSACTION.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_TRANSACTION_BYTES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_UNCONFIRMED_TRANSACTION_IDS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_UNCONFIRMED_TRANSACTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EXPECTED_TRANSACTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_CURRENT_ASK_ORDER_IDS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_CURRENT_BID_ORDER_IDS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_CURRENT_ASK_ORDERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_CURRENT_BID_ORDERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ALL_OPEN_ASK_ORDERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ALL_OPEN_BID_ORDERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_BUY_OFFERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EXPECTED_BUY_OFFERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_SELL_OFFERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EXPECTED_SELL_OFFERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_OFFER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_AVAILABLE_TO_BUY.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_AVAILABLE_TO_SELL.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ASK_ORDER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ASK_ORDER_IDS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ASK_ORDERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_BID_ORDER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_BID_ORDER_IDS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_BID_ORDERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EXPECTED_ASK_ORDERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EXPECTED_BID_ORDERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EXPECTED_ORDER_CANCELLATIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ORDER_TRADES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_EXCHANGE_REQUESTS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EXPECTED_EXCHANGE_REQUESTS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_MINTING_TARGET.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ALL_SHUFFLINGS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_SHUFFLINGS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ASSIGNED_SHUFFLINGS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_HOLDING_SHUFFLINGS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_SHUFFLING.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_SHUFFLING_PARTICIPANTS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_PRUNABLE_MESSAGE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_PRUNABLE_MESSAGES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ALL_PRUNABLE_MESSAGES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.VERIFY_PRUNABLE_MESSAGE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.ISSUE_ASSET.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.ISSUE_CURRENCY.getHandler().canHaveRecipient());
        Assert.assertTrue(APIEnum.LEASE_BALANCE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.LONG_CONVERT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.HEX_CONVERT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.PARSE_TRANSACTION.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.PLACE_ASK_ORDER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.PLACE_BID_ORDER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.PUBLISH_EXCHANGE_OFFER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.RS_CONVERT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.READ_MESSAGE.getHandler().canHaveRecipient());
        Assert.assertTrue(APIEnum.SEND_MESSAGE.getHandler().canHaveRecipient());
        Assert.assertTrue(APIEnum.SEND_MONEY.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SET_ACCOUNT_INFO.getHandler().canHaveRecipient());
        Assert.assertTrue(APIEnum.SET_ACCOUNT_PROPERTY.getHandler().canHaveRecipient());
        Assert.assertTrue(APIEnum.DELETE_ACCOUNT_PROPERTY.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SET_ALIAS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SHUFFLING_CREATE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SHUFFLING_REGISTER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SHUFFLING_PROCESS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SHUFFLING_VERIFY.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SHUFFLING_CANCEL.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.START_SHUFFLER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.STOP_SHUFFLER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_SHUFFLERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DELETE_ALIAS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SIGN_TRANSACTION.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.START_FORGING.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.STOP_FORGING.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_FORGING.getHandler().canHaveRecipient());
        Assert.assertTrue(APIEnum.TRANSFER_ASSET.getHandler().canHaveRecipient());
        Assert.assertTrue(APIEnum.TRANSFER_CURRENCY.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.CAN_DELETE_CURRENCY.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DELETE_CURRENCY.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DIVIDEND_PAYMENT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SEARCH_DGS_GOODS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SEARCH_ASSETS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SEARCH_CURRENCIES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SEARCH_POLLS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SEARCH_ACCOUNTS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SEARCH_TAGGED_DATA.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.UPLOAD_TAGGED_DATA.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_TAGGED_DATA.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ALL_TAGGED_DATA.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_CHANNEL_TAGGED_DATA.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_TAGGED_DATA.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DOWNLOAD_TAGGED_DATA.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_DATA_TAGS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_DATA_TAG_COUNT.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_DATA_TAGS_LIKE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.VERIFY_TAGGED_DATA.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.CLEAR_UNCONFIRMED_TRANSACTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.REQUEUE_UNCONFIRMED_TRANSACTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.REBROADCAST_UNCONFIRMED_TRANSACTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ALL_WAITING_TRANSACTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ALL_BROADCASTED_TRANSACTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.FULL_RESET.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.POP_OFF.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SCAN.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.LUCENE_REINDEX.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.ADD_PEER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.BLACKLIST_PEER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DUMP_PEERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_LOG.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_STACK_TRACES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.RETRIEVE_PRUNED_DATA.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.RETRIEVE_PRUNED_TRANSACTION.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SET_LOGGING.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SHUTDOWN.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.TRIM_DERIVED_TABLES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.HASH.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.FULL_HASH_TO_ID.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SET_PHASING_ONLY_CONTROL.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_PHASING_ONLY_CONTROL.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ALL_PHASING_ONLY_CONTROLS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DETECT_MIME_TYPE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.START_FUNDING_MONITOR.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.STOP_FUNDING_MONITOR.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_FUNDING_MONITOR.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DOWNLOAD_PRUNABLE_MESSAGE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_SHARED_KEY.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SET_API_PROXY_PEER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SEND_TRANSACTION.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ASSET_DIVIDENDS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.BLACKLIST_API_PROXY_PEER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_NEXT_BLOCK_GENERATORS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.START_BUNDLER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.STOP_BUNDLER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_BUNDLERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.BUNDLE_TRANSACTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.EXCHANGE_COINS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.CANCEL_COIN_EXCHANGE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_COIN_EXCHANGE_ORDER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_COIN_EXCHANGE_ORDER_IDS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_COIN_EXCHANGE_ORDERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_COIN_EXCHANGE_TRADES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_COIN_EXCHANGE_TRADE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EXPECTED_COIN_EXCHANGE_ORDERS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EXPECTED_COIN_EXCHANGE_ORDER_CANCELLATIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_BUNDLER_RATES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_FXT_TRANSACTION.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_BALANCES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SIMULATE_COIN_EXCHANGE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EFFECTIVE_BALANCE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.BLACKLIST_BUNDLER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ALL_BUNDLER_RATES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.EVALUATE_EXPRESSION.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.PARSE_PHASING_PARAMS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SET_PHASING_ASSET_CONTROL.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_PHASING_ASSET_CONTROL.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.INCREASE_ASSET_SHARES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EXECUTED_TRANSACTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.ADD_BUNDLING_RULE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_BUNDLING_OPTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.PROCESS_VOUCHER.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SET_CONTRACT_REFERENCE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DELETE_CONTRACT_REFERENCE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_CONTRACT_REFERENCES.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.CALCULATE_FEE.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ASSET_PROPERTIES.getHandler().canHaveRecipient());
        Assert.assertTrue(APIEnum.SET_ASSET_PROPERTY.getHandler().canHaveRecipient());
        Assert.assertTrue(APIEnum.DELETE_ASSET_PROPERTY.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_HASHED_SECRET_PHASED_TRANSACTIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SPLIT_SECRET.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.COMBINE_SECRET.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.MANAGE_PEERS_NETWORKING.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_LEDGER_MASTER_PUBLIC_KEY.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DERIVE_ACCOUNT_FROM_MASTER_PUBLIC_KEY.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.DERIVE_ACCOUNT_FROM_SEED.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_ACCOUNT_PERMISSIONS.getHandler().canHaveRecipient());
        Assert.assertTrue(APIEnum.ADD_ACCOUNT_PERMISSION.getHandler().canHaveRecipient());
        Assert.assertTrue(APIEnum.REMOVE_ACCOUNT_PERMISSION.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_CHAIN_PERMISSIONS.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_EPOCH_TIME.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.BOOTSTRAP_API_PROXY.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_CONFIGURATION.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.SET_CONFIGURATION.getHandler().canHaveRecipient());
        Assert.assertFalse(APIEnum.GET_API_PROXY_REPORTS.getHandler().canHaveRecipient());
    }
}

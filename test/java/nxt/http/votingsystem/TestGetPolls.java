/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2021 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of this software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package nxt.http.votingsystem;

import nxt.BlockchainTest;
import nxt.addons.JO;
import nxt.http.callers.CreatePollCall;
import nxt.http.callers.GetPollsCall;
import nxt.util.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import static nxt.blockchain.ChildChain.IGNIS;

public class TestGetPolls extends BlockchainTest {

    @Test
    public void accountPollsIncrease() {
        GetPollsCall getPollsCall = GetPollsCall.create(IGNIS.getId())
                .account(DAVE.getId())
                .firstIndex(0)
                .lastIndex(100);

        JO jsonResponse = getPollsCall.callNoError();
        Logger.logMessage("getPollsResponse:" + jsonResponse.toJSONString());
        JSONArray polls = (JSONArray) jsonResponse.get("polls");
        int initialSize = polls.size();

        CreatePollCall createPollCall = TestCreatePoll.createPollBuilder().secretPhrase(DAVE.getSecretPhrase());
        String poll = TestCreatePoll.issueCreatePoll(createPollCall, false);
        generateBlock();

        jsonResponse = getPollsCall.callNoError();
        Logger.logMessage("getPollsResponse:" + jsonResponse.toJSONString());
        polls = (JSONArray) jsonResponse.get("polls");
        int size = polls.size();

        JSONObject lastPoll = (JSONObject) polls.get(0);
        Assert.assertEquals(poll, lastPoll.get("poll"));
        Assert.assertEquals(size, initialSize + 1);
    }
}
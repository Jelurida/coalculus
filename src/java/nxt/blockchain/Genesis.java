/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2021 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of this software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package nxt.blockchain;

import nxt.Constants;
import nxt.account.Account;
import nxt.account.AccountRestrictions;
import nxt.ae.Asset;
import nxt.aliases.AliasHome;
import nxt.crypto.Crypto;
import nxt.dbschema.Db;
import nxt.ms.Currency;
import nxt.util.Convert;
import nxt.util.Logger;
import nxt.util.ResourceLookup;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public final class Genesis {

    static final byte[] generationSignature = Constants.isTestnet ?
            new byte[] {
                    114, 63, -55, -13, 92, 68, 88, -8, 7, 126, -25, 118, -93, 103, 3, -16,
                    -110, -72, -128, 58, 86, -33, 38, 34, 38, 81, -29, 5, -30, 43, 19, -66
            }
            :
            new byte[] {
                    -5, 7, -122, 86, -124, 23, 91, -77, -109, 0, 101, 89, 115, -103, -5, 107,
                    -9, -121, 18, -123, -48, 109, -33, 48, 72, 46, -72, 56, -120, -23, -17, -6
            };

    static byte[] apply() {
        MessageDigest digest = Crypto.sha256();
        importBalances(digest);
        importAliases(digest);
        importAssets(digest);
        importCurrencies(digest);
        importAccountInfo(digest);
        importAccountProperties(digest);
        importAccountControls(digest);
        digest.update(Convert.toBytes(Constants.EPOCH_BEGINNING));
        return digest.digest();
    }

    private static void importBalances(MessageDigest digest) {
        List<Chain> chains = new ArrayList<>();
        ChildChain.getAll().forEach(childChain -> {
            if (childChain.isEnabled()) {
                chains.add(childChain);
            }
        });
        chains.add(FxtChain.FXT);
        chains.sort(Comparator.comparingInt(Chain::getId));
        for (Chain chain : chains) {
            try (InputStreamReader is = new InputStreamReader(new DigestInputStream(
                    ResourceLookup.getSystemResourceAsStream("data/" + chain.getName() + (Constants.isTestnet ? "-testnet.json" : ".json")), digest), "UTF-8")) {
                JSONObject chainBalances = (JSONObject) JSONValue.parseWithException(is);
                loadBalances(chain, chainBalances);
            } catch (IOException|ParseException e) {
                throw new RuntimeException("Failed to process genesis recipients accounts for " + chain.getName(), e);
            }
        }
    }

    public static void loadBalances(Chain chain, Map<String, Long> chainBalances) {
        Logger.logDebugMessage("Loading balances for chain %s", chain.getName());
        int count = 0;
        long total = 0;
        for (Map.Entry<String, Long> entry : chainBalances.entrySet()) {
            long quantity = entry.getValue();
            String key = entry.getKey();
            Account account;
            if (key.length() == 64) {
                byte[] publicKey = Convert.parseHexString(key);
                if (!Crypto.isCanonicalPublicKey(publicKey)) {
                    Logger.logErrorMessage("Public key is not canonical: " + Convert.toHexString(publicKey));
                    continue;
                }
                account = Account.addOrGetAccount(Account.getId(publicKey));
                try {
                    account.apply(publicKey);
                } catch (IllegalStateException e) {
                    Logger.logErrorMessage(String.format("Public key mismatch for account %s", Long.toUnsignedString(account.getId())), e);
                }
            } else {
                account = Account.addOrGetAccount(Long.parseUnsignedLong(key));
            }
            account.addToBalanceAndUnconfirmedBalance(chain, null, null, quantity);
            total += quantity;
            if (count++ % Constants.BATCH_COMMIT_SIZE == 0) {
                Db.db.commitTransaction();
                Db.db.clearCache();
            }
        }
        Logger.logDebugMessage("Total balance %f %s (%d NQT)", (double)total / chain.ONE_COIN, chain.getName(), total);
    }

    private static void importAliases(MessageDigest digest) {
		InputStream stream = ResourceLookup.getSystemResourceAsStream("data/IGNIS_ALIASES" + (Constants.isTestnet ? "-testnet.json" : ".json"));
        if (stream == null) {
            Logger.logDebugMessage("No aliases to import");
            return;
        }
        try (InputStreamReader reader = new InputStreamReader(new DigestInputStream(stream, digest), "UTF-8")) {
            JSONObject aliases = (JSONObject) JSONValue.parseWithException(reader);
            Logger.logDebugMessage("Loading aliases");
            int count = 0;
            long aliasId = 1;
            AliasHome ignisAliasHome = ChildChain.IGNIS.getAliasHome();
            for (Map.Entry<String, Map<String, String>> entry : ((Map<String, Map<String, String>>)aliases).entrySet()) {
                String aliasName = entry.getKey();
                String aliasURI = entry.getValue().get("uri");
                long accountId = Long.parseUnsignedLong(entry.getValue().get("account"));
                ignisAliasHome.importAlias(aliasId++, accountId, aliasName, aliasURI);
                if (count++ % Constants.BATCH_COMMIT_SIZE == 0) {
                    Db.db.commitTransaction();
                    Db.db.clearCache();
                }
            }
            Logger.logDebugMessage("Loaded " + count + " aliases");
        } catch (IOException|ParseException e) {
            throw new RuntimeException("Failed to process aliases", e);
        }
    }

    private static void importAssets(MessageDigest digest) {
        InputStream stream = ResourceLookup.getSystemResourceAsStream("data/ASSETS" + (Constants.isTestnet ? "-testnet.json" : ".json"));
        if (stream == null) {
            Logger.logDebugMessage("No assets to import");
            return;
        }
        try (InputStreamReader reader = new InputStreamReader(new DigestInputStream(stream, digest), "UTF-8")) {
            JSONObject assets = (JSONObject) JSONValue.parseWithException(reader);
            Logger.logDebugMessage("Loading assets");
            int count = 0;
            for (Map.Entry<String, Map<String, Object>> entry : ((Map<String, Map<String, Object>>)assets).entrySet()) {
                long assetId = Long.parseUnsignedLong(entry.getKey());
                Map<String, Object> asset = entry.getValue();
                String name = (String)asset.get("name");
                String description = (String)asset.get("description");
                byte decimals = ((Long)asset.get("decimals")).byteValue();
                long issuerId = Long.parseUnsignedLong((String)asset.get("issuer"));
                Map<String, Long> assetBalances = (Map<String, Long>)asset.get("balances");
                long total = 0;
                for (Map.Entry<String, Long> balanceEntry : assetBalances.entrySet()) {
                    Account account = Account.addOrGetAccount(Long.parseUnsignedLong(balanceEntry.getKey()));
                    long quantityQNT = balanceEntry.getValue();
                    account.addToAssetAndUnconfirmedAssetBalanceQNT(null, null, assetId, quantityQNT);
                    total += quantityQNT;
                    if (count++ % Constants.BATCH_COMMIT_SIZE == 0) {
                        Db.db.commitTransaction();
                        Db.db.clearCache();
                    }
                }
                Asset.importAsset(assetId, issuerId, name, description, decimals, total);
            }
            Logger.logDebugMessage("Loaded " + count + " assets");
        } catch (IOException|ParseException e) {
            throw new RuntimeException("Failed to process assets", e);
        }
    }

    private static void importCurrencies(MessageDigest digest) {
        InputStream stream = ResourceLookup.getSystemResourceAsStream("data/IGNIS_CURRENCIES" + (Constants.isTestnet ? "-testnet.json" : ".json"));
        if (stream == null) {
            Logger.logDebugMessage("No currencies to import");
            return;
        }
        try (InputStreamReader reader = new InputStreamReader(new DigestInputStream(stream, digest), "UTF-8")) {
            JSONObject currencies = (JSONObject) JSONValue.parseWithException(reader);
            Logger.logDebugMessage("Loading currencies");
            int count = 0;
            long currencyId = 1;
            for (Map.Entry<String, Map<String, String>> entry : ((Map<String, Map<String, String>>)currencies).entrySet()) {
                String currencyCode = entry.getKey();
                String currencyName = entry.getValue().get("name");
                long accountId = Long.parseUnsignedLong(entry.getValue().get("account"));
                Account account = Account.addOrGetAccount(accountId);
                account.addToCurrencyAndUnconfirmedCurrencyUnits(null, null, currencyId, 1);
                Currency.importCurrency(currencyId++, accountId, currencyCode, currencyName);
                if (count++ % Constants.BATCH_COMMIT_SIZE == 0) {
                    Db.db.commitTransaction();
                    Db.db.clearCache();
                }
            }
            Logger.logDebugMessage("Loaded " + count + " currencies");
        } catch (IOException|ParseException e) {
            throw new RuntimeException("Failed to process currencies", e);
        }
    }

    private static void importAccountInfo(MessageDigest digest) {
        InputStream stream = ResourceLookup.getSystemResourceAsStream("data/ACCOUNT_INFO" + (Constants.isTestnet ? "-testnet.json" : ".json"));
        if (stream == null) {
            Logger.logDebugMessage("No account info to import");
            return;
        }
        try (InputStreamReader reader = new InputStreamReader(new DigestInputStream(stream, digest), "UTF-8")) {
            JSONObject accountInfos = (JSONObject) JSONValue.parseWithException(reader);
            Logger.logDebugMessage("Loading account info");
            int count = 0;
            for (Map.Entry<String, Map<String, String>> entry : ((Map<String, Map<String, String>>)accountInfos).entrySet()) {
                long accountId = Long.parseUnsignedLong(entry.getKey());
                String name = entry.getValue().get("name");
                String description = entry.getValue().get("description");
                Account.getAccount(accountId).setAccountInfo(name, description);
                if (count++ % Constants.BATCH_COMMIT_SIZE == 0) {
                    Db.db.commitTransaction();
                    Db.db.clearCache();
                }
            }
            Logger.logDebugMessage("Loaded " + count + " account infos");
        } catch (IOException|ParseException e) {
            throw new RuntimeException("Failed to process account infos", e);
        }
    }

    private static void importAccountProperties(MessageDigest digest) {
        InputStream stream = ResourceLookup.getSystemResourceAsStream("data/ACCOUNT_PROPERTIES" + (Constants.isTestnet ? "-testnet.json" : ".json"));
        if (stream == null) {
            Logger.logDebugMessage("No account properties to import");
            return;
        }
        try (InputStreamReader reader = new InputStreamReader(new DigestInputStream(stream, digest), "UTF-8")) {
            JSONObject accountProperties = (JSONObject) JSONValue.parseWithException(reader);
            Logger.logDebugMessage("Loading account properties");
            int count = 0;
            long propertyId = 1;
            for (Map.Entry<String, Map<String, Map<String, String>>> entry : ((Map<String, Map<String, Map<String, String>>>)accountProperties).entrySet()) {
                long recipientId = Long.parseUnsignedLong(entry.getKey());
                Map<String, Map<String, String>> setters = entry.getValue();
                for (Map.Entry<String, Map<String, String>> setterEntry : setters.entrySet()) {
                    long setterId = Long.parseUnsignedLong(setterEntry.getKey());
                    Map<String, String> setterProperties = setterEntry.getValue();
                    for (Map.Entry<String, String> property : setterProperties.entrySet()) {
                        String name = property.getKey();
                        String value = property.getValue();
                        Account.importProperty(propertyId++, recipientId, setterId, name, value);
                    }
                }
                if (count++ % Constants.BATCH_COMMIT_SIZE == 0) {
                    Db.db.commitTransaction();
                    Db.db.clearCache();
                }
            }
            Logger.logDebugMessage("Loaded " + count + " account properties");
        } catch (IOException|ParseException e) {
            throw new RuntimeException("Failed to process account properties", e);
        }
    }

    private static void importAccountControls(MessageDigest digest) {
        InputStream stream = ResourceLookup.getSystemResourceAsStream("data/ACCOUNT_CONTROL" + (Constants.isTestnet ? "-testnet.json" : ".json"));
        if (stream == null) {
            Logger.logDebugMessage("No account controls to import");
            return;
        }
        try (InputStreamReader reader = new InputStreamReader(new DigestInputStream(stream, digest), "UTF-8")) {
            JSONObject accountControls = (JSONObject) JSONValue.parseWithException(reader);
            Logger.logDebugMessage("Loading account controls");
            int count = 0;
            for (Map.Entry<String, Map<String, Object>> entry : ((Map<String, Map<String, Object>>)accountControls).entrySet()) {
                long accountId = Long.parseUnsignedLong(entry.getKey());
                int quorum = ((Long)entry.getValue().get("quorum")).intValue();
                long maxFees = (Long) entry.getValue().get("maxFees");
                int minDuration = ((Long)entry.getValue().get("minDuration")).intValue();
                int maxDuration = ((Long)entry.getValue().get("maxDuration")).intValue();
                JSONArray whitelist = (JSONArray)entry.getValue().get("whitelist");
                AccountRestrictions.PhasingOnly.importPhasingOnly(accountId, Convert.toArray(whitelist), quorum, maxFees, minDuration, maxDuration);
                if (count++ % Constants.BATCH_COMMIT_SIZE == 0) {
                    Db.db.commitTransaction();
                    Db.db.clearCache();
                }
            }
            Logger.logDebugMessage("Loaded " + count + " account controls");
        } catch (IOException|ParseException e) {
            throw new RuntimeException("Failed to process account controls", e);
        }
    }


    private Genesis() {} // never

}

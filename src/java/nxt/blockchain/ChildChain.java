/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2021 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of this software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package nxt.blockchain;

import nxt.Constants;
import nxt.Nxt;
import nxt.NxtException;
import nxt.ae.AssetDividendHome;
import nxt.ae.AssetExchangeTransactionType;
import nxt.ae.OrderHome;
import nxt.ae.TradeHome;
import nxt.aliases.AliasHome;
import nxt.aliases.AliasTransactionType;
import nxt.blockchain.chaincontrol.PermissionChecker;
import nxt.blockchain.chaincontrol.PermissionPolicy;
import nxt.blockchain.chaincontrol.PermissionPolicyType;
import nxt.blockchain.chaincontrol.PermissionReader;
import nxt.blockchain.chaincontrol.PermissionWriter;
import nxt.dgs.DigitalGoodsHome;
import nxt.http.APIEnum;
import nxt.http.APITag;
import nxt.lightcontracts.LightContractTransactionType;
import nxt.ms.CurrencyFounderHome;
import nxt.ms.ExchangeHome;
import nxt.ms.ExchangeOfferHome;
import nxt.ms.ExchangeRequestHome;
import nxt.ms.MonetarySystemTransactionType;
import nxt.shuffling.ShufflingHome;
import nxt.shuffling.ShufflingParticipantHome;
import nxt.shuffling.ShufflingTransactionType;
import nxt.taggeddata.TaggedDataHome;
import nxt.util.Convert;
import nxt.voting.PhasingPollHome;
import nxt.voting.PhasingVoteHome;
import nxt.voting.PollHome;
import nxt.voting.VoteHome;
import nxt.voting.VotingTransactionType;
import org.json.simple.JSONObject;

import java.nio.ByteBuffer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BooleanSupplier;

import static nxt.blockchain.chaincontrol.PermissionPolicyType.CHILD_CHAIN;

import java.util.function.Supplier;
import java.util.stream.Stream;

public class ChildChain extends Chain {

    private static final Map<String, ChildChain> childChains = new HashMap<>();
    private static final Map<Integer, ChildChain> childChainsById = new HashMap<>();

    private static final Collection<ChildChain> allChildChains = Collections.unmodifiableCollection(childChains.values());

    private static final Long[] testnetAdminAccounts =
            Constants.isAutomatedTest ?
                    new Long[] {
                            Convert.parseAccountId("COAL-44VB-6KTB-EDQL-3HH6Q"),
                            Convert.parseAccountId("COAL-YDLQ-YKY4-7FRJ-2CERN")
                    }
                    :
                    new Long[] {
                            Convert.parseAccountId("COAL-44VB-6KTB-EDQL-3HH6Q"),
                            Convert.parseAccountId("COAL-YDLQ-YKY4-7FRJ-2CERN"),
                            Convert.parseAccountId("COAL-NZD6-X586-NHG2-H8JGT"),
                            Convert.parseAccountId("COAL-6W3P-C957-66F3-ETFQ9"),
                            Convert.parseAccountId("COAL-VLAG-7JBQ-885P-HXS6L"),
                            Convert.parseAccountId("COAL-BZD7-M66P-33QS-FXY5J"),
                            Convert.parseAccountId("COAL-3TA9-ENJQ-QEBQ-82U7V"),
                            Convert.parseAccountId("COAL-66MG-6P2V-UDU8-HBFGH"),
                            Convert.parseAccountId("COAL-DVGQ-NDWC-A8TG-38LM8"),
                            Convert.parseAccountId("COAL-XQGK-LFAA-A358-47XH9")
                    };

    private static final Long[] UST_ADMIN_ACCOUNTS = new Long[] {
            Convert.parseAccountId("COAL-LBVD-DWTF-JP9R-7V2PP"),
            Convert.parseAccountId("COAL-56LK-M7US-U7U3-C27BL"),
            Convert.parseAccountId("COAL-5C4Q-ZNFC-66M7-C676M"),
            Convert.parseAccountId("COAL-Z4T9-P9PG-WMHW-DZL2G"),
            Convert.parseAccountId("COAL-XUH6-ZNJZ-QCAB-G6VTJ")
    };

    private static final Long[] SGT_ADMIN_ACCOUNTS = new Long[] {
            Convert.parseAccountId("COAL-CDNB-TJ99-LY95-46UY2"),
            Convert.parseAccountId("COAL-RVLN-FH9G-BJN7-CLP8K"),
            Convert.parseAccountId("COAL-XVWE-S6N2-N43F-7GGW2"),
            Convert.parseAccountId("COAL-HUWH-MZFP-2JYU-ENNKJ"),
            Convert.parseAccountId("COAL-7XXU-G72V-4ASP-2UUVY")
    };

    private static final Long[] MYR_ADMIN_ACCOUNTS = new Long[] {
            Convert.parseAccountId("COAL-NX3U-AHL3-3KKG-5YX7J"),
            Convert.parseAccountId("COAL-W5F3-PY6Z-QCKT-FY86M"),
            Convert.parseAccountId("COAL-JSQW-MW4W-F4AJ-5RZJT"),
            Convert.parseAccountId("COAL-XWVM-E4WZ-83AV-AV6KH"),
            Convert.parseAccountId("COAL-67YQ-L4HX-2LD7-BF4HH")
    };

    private static final Long[] ASEAN_ADMIN_ACCOUNTS = new Long[] {
            Convert.parseAccountId("COAL-C7RX-X8FS-BWAK-9W3W4"),
            Convert.parseAccountId("COAL-BSDV-SUK9-2J5S-HXWT6"),
            Convert.parseAccountId("COAL-DS4T-7SF8-4J9W-F3CXM"),
            Convert.parseAccountId("COAL-4XT7-6XVX-TULN-FW9HF"),
            Convert.parseAccountId("COAL-FSZD-E48H-2448-B36D7")
    };

    private static final Long[] EBMA_ADMIN_ACCOUNTS = new Long[] {
            Convert.parseAccountId("COAL-2Z5D-R2GT-7A2U-88LPV"),
            Convert.parseAccountId("COAL-PVU5-6MUC-WP7F-2ANXC"),
            Convert.parseAccountId("COAL-M9MD-T8DZ-EUKM-2JNSD"),
            Convert.parseAccountId("COAL-UVLB-UKKV-C4HE-8DTUB"),
            Convert.parseAccountId("COAL-LBM2-UT66-XVSG-EBXNB")
    };

    public static final ChildChain UST = new ChildChainBuilder(2, "UST")
            .setTotalAmount(100_000_000_000_000000L)
            .setPermissionPolicy(CHILD_CHAIN)
            .setMasterAdminAccounts(Constants.isTestnet ? testnetAdminAccounts : UST_ADMIN_ACCOUNTS)
            .build();

    public static final ChildChain SGT = new ChildChainBuilder(3, "SGT")
            .setTotalAmount(100_000_000_000_000000L)
            .setPermissionPolicy(CHILD_CHAIN)
            .setMasterAdminAccounts(Constants.isTestnet ? testnetAdminAccounts : SGT_ADMIN_ACCOUNTS)
            .build();

    public static final ChildChain MYR = new ChildChainBuilder(4, "MYR")
            .setTotalAmount(1_000_000_000_000_000000L)
            .setPermissionPolicy(CHILD_CHAIN)
            .setMasterAdminAccounts(Constants.isTestnet ? testnetAdminAccounts : MYR_ADMIN_ACCOUNTS)
            .build();

    public static final ChildChain ASEAN = new ChildChainBuilder(5, "ASEAN")
            .setTotalAmount(1_000_000_000_000_000000L)
            .setPermissionPolicy(CHILD_CHAIN)
            .setMasterAdminAccounts(Constants.isTestnet ? testnetAdminAccounts : ASEAN_ADMIN_ACCOUNTS)
            .build();

    public static final ChildChain EBMA = new ChildChainBuilder(6, "eBmA")
            .setTotalAmount(100_000_000_000_000000L)
            .setPermissionPolicy(CHILD_CHAIN)
            .setMasterAdminAccounts(Constants.isTestnet ? testnetAdminAccounts : EBMA_ADMIN_ACCOUNTS)
            .build();

    private static final Long[] GST_TESTNET_ADMIN_ACCOUNTS = Stream.concat(Arrays.stream(testnetAdminAccounts),
            Stream.of("COAL-42PE-QGRW-73WX-FF8A2", "COAL-RYKU-NC7U-7RUJ-DE756").map(Convert::parseAccountId)).toArray(Long[]::new);

    private static final Long[] GUSD_ADMIN_ACCOUNTS = new Long[]{
            Convert.parseAccountId("COAL-LKVP-KR8X-ZHPY-3CMK2"),
            Convert.parseAccountId("COAL-DR85-SBAL-U4PT-9HCE9"),
            Convert.parseAccountId("COAL-3AJP-6NU9-CYWV-A9YSE"),
            Convert.parseAccountId("COAL-U444-HSCQ-5Y2C-5DF6N"),
            Convert.parseAccountId("COAL-QU9S-KX9G-2GXP-GSHPD"),
            Convert.parseAccountId("COAL-CKMW-HB77-K4W8-7EMNP"),
            Convert.parseAccountId("COAL-EPGN-3T5P-ZFCK-FT5TG"),
    };

    private static final Long[] GEUR_ADMIN_ACCOUNTS = new Long[]{
            Convert.parseAccountId("COAL-LKVP-KR8X-ZHPY-3CMK2"),
            Convert.parseAccountId("COAL-DR85-SBAL-U4PT-9HCE9"),
            Convert.parseAccountId("COAL-YKKH-UC59-M94A-CVSRT"),
            Convert.parseAccountId("COAL-DQB2-N5Y9-NZ9V-D73N6"),
            Convert.parseAccountId("COAL-9VL3-4LYL-CAJ5-HAN9L"),
            Convert.parseAccountId("COAL-5ZBC-7SXX-E9PH-5GWQN"),
            Convert.parseAccountId("COAL-XG6B-N76A-SCYQ-86HX6"),
    };

    private static final Long[] GGBP_ADMIN_ACCOUNTS = new Long[]{
            Convert.parseAccountId("COAL-LKVP-KR8X-ZHPY-3CMK2"),
            Convert.parseAccountId("COAL-DR85-SBAL-U4PT-9HCE9"),
            Convert.parseAccountId("COAL-E65E-4WWH-N7DN-2H6S4"),
            Convert.parseAccountId("COAL-UBFK-GHDH-VPE2-7QU68"),
            Convert.parseAccountId("COAL-6BFK-3F8D-RGV9-E3N45"),
            Convert.parseAccountId("COAL-DZ8C-WHJU-LFKS-HUKNE"),
            Convert.parseAccountId("COAL-WE39-72TP-H3SP-B5YNJ"),
    };

    private static final Long[] GRMB_ADMIN_ACCOUNTS = new Long[]{
            Convert.parseAccountId("COAL-LKVP-KR8X-ZHPY-3CMK2"),
            Convert.parseAccountId("COAL-DR85-SBAL-U4PT-9HCE9"),
            Convert.parseAccountId("COAL-3SD9-EL49-DDY3-2LH2Q"),
            Convert.parseAccountId("COAL-B9NF-5TFS-F78Z-4YEHR"),
            Convert.parseAccountId("COAL-HW22-YG9U-UQZU-43UJG"),
            Convert.parseAccountId("COAL-SNAM-DJRS-6KJP-BS47L"),
            Convert.parseAccountId("COAL-RYQX-NA57-TX66-7AZRV"),
    };

    public static final ChildChain GUSD = new ChildChainBuilder(7, "GUSD")
            .setTotalAmount(10_000_000_000_000000L)
            .setPermissionPolicy(CHILD_CHAIN)
            .setMasterAdminAccounts(Constants.isTestnet ? GST_TESTNET_ADMIN_ACCOUNTS : GUSD_ADMIN_ACCOUNTS)
            .setIsEnabled(() -> Nxt.getBlockchain().getHeight() >= Constants.UPGRADE_TO_2_3_4_BLOCK)
            .build();

    public static final ChildChain GEUR = new ChildChainBuilder(8, "GEUR")
            .setTotalAmount(10_000_000_000_000000L)
            .setPermissionPolicy(CHILD_CHAIN)
            .setMasterAdminAccounts(Constants.isTestnet ? GST_TESTNET_ADMIN_ACCOUNTS : GEUR_ADMIN_ACCOUNTS)
            .setIsEnabled(() -> Nxt.getBlockchain().getHeight() >= Constants.UPGRADE_TO_2_3_4_BLOCK)
            .build();

    public static final ChildChain GGBP = new ChildChainBuilder(9, "GGBP")
            .setTotalAmount(10_000_000_000_000000L)
            .setPermissionPolicy(CHILD_CHAIN)
            .setMasterAdminAccounts(Constants.isTestnet ? GST_TESTNET_ADMIN_ACCOUNTS : GGBP_ADMIN_ACCOUNTS)
            .setIsEnabled(() -> Nxt.getBlockchain().getHeight() >= Constants.UPGRADE_TO_2_3_4_BLOCK)
            .build();

    public static final ChildChain GRMB = new ChildChainBuilder(10, "GRMB")
            .setTotalAmount(10_000_000_000_000000L)
            .setPermissionPolicy(CHILD_CHAIN)
            .setMasterAdminAccounts(Constants.isTestnet ? GST_TESTNET_ADMIN_ACCOUNTS : GRMB_ADMIN_ACCOUNTS)
            .setIsEnabled(() -> Nxt.getBlockchain().getHeight() >= Constants.UPGRADE_TO_2_3_4_BLOCK)
            .build();

    private static final Long[] DASH_TESTNET_ADMIN_ACCOUNTS = Stream.concat(Arrays.stream(testnetAdminAccounts),
            Stream.of("COAL-LZBM-MFHL-GUR8-BXN9M", "COAL-NU35-XW2D-KEV8-2EGW4").map(Convert::parseAccountId)).toArray(Long[]::new);

    private static final Long[] DUSD_ADMIN_ACCOUNTS = new Long[]{
            Convert.parseAccountId("COAL-C29Z-QUR6-TEDT-86UAU"),
            Convert.parseAccountId("COAL-HSJC-GFU3-BVZK-6V957"),
            Convert.parseAccountId("COAL-TG3J-9B2V-BSXK-HK2RW"),
            Convert.parseAccountId("COAL-MYXF-ZFQF-M5HV-CPEHC"),
            Convert.parseAccountId("COAL-C47X-VX6E-ADJH-37TZ8"),
            Convert.parseAccountId("COAL-HXX2-U4DL-NL6T-3TFXQ"),
            Convert.parseAccountId("COAL-N5PZ-9NVD-J73Y-99PSE"),
    };

    private static final Long[] DEUR_ADMIN_ACCOUNTS = new Long[]{
            Convert.parseAccountId("COAL-C29Z-QUR6-TEDT-86UAU"),
            Convert.parseAccountId("COAL-HSJC-GFU3-BVZK-6V957"),
            Convert.parseAccountId("COAL-XKHY-R6FR-WD6C-FSY69"),
            Convert.parseAccountId("COAL-NY6Q-782V-GBX9-B8268"),
            Convert.parseAccountId("COAL-VDXN-APM8-CAHJ-E6Y7P"),
            Convert.parseAccountId("COAL-2ZFA-Z9RN-XZVG-943X9"),
            Convert.parseAccountId("COAL-7K2P-P7M3-GCFX-GH22L"),
    };

    private static final Long[] DCAD_ADMIN_ACCOUNTS = new Long[]{
            Convert.parseAccountId("COAL-C29Z-QUR6-TEDT-86UAU"),
            Convert.parseAccountId("COAL-HSJC-GFU3-BVZK-6V957"),
            Convert.parseAccountId("COAL-U47H-K3LR-ZDTL-AJQJE"),
            Convert.parseAccountId("COAL-VKZN-X3AN-CX7X-E26LZ"),
            Convert.parseAccountId("COAL-U9RB-6DQ2-RF6Z-BXYN9"),
            Convert.parseAccountId("COAL-VSTN-Y8B2-7NSW-3E7R2"),
            Convert.parseAccountId("COAL-95WS-7RWU-P7X7-H6AYX"),
    };

    private static final Long[] DRMB_ADMIN_ACCOUNTS = new Long[]{
            Convert.parseAccountId("COAL-C29Z-QUR6-TEDT-86UAU"),
            Convert.parseAccountId("COAL-HSJC-GFU3-BVZK-6V957"),
            Convert.parseAccountId("COAL-QEJE-KZZ7-K33P-BPE65"),
            Convert.parseAccountId("COAL-BRGT-MEVV-J7FN-6M36X"),
            Convert.parseAccountId("COAL-X5AL-MH43-F282-97LSU"),
            Convert.parseAccountId("COAL-A4Y7-X3R6-VUDY-3FMR8"),
            Convert.parseAccountId("COAL-EALT-YUAU-HQYT-ACF54"),
    };

    private static final Long[] DSGD_ADMIN_ACCOUNTS = new Long[]{
            Convert.parseAccountId("COAL-C29Z-QUR6-TEDT-86UAU"),
            Convert.parseAccountId("COAL-HSJC-GFU3-BVZK-6V957"),
            Convert.parseAccountId("COAL-YGT5-FWL8-THW8-49QRE"),
            Convert.parseAccountId("COAL-9NUD-QD2E-HTRY-FK3E8"),
            Convert.parseAccountId("COAL-VHX3-NK9Q-VGKW-H534C"),
            Convert.parseAccountId("COAL-A5NX-B6SD-MQ2M-4ZCQ4"),
            Convert.parseAccountId("COAL-T7F9-KAM9-8XYX-AJHV5"),
    };

    public static final ChildChain DUSD = new ChildChainBuilder(11, "DUSD")
            .setTotalAmount(10_000_000_000_000000L)
            .setPermissionPolicy(CHILD_CHAIN)
            .setMasterAdminAccounts(Constants.isTestnet ? DASH_TESTNET_ADMIN_ACCOUNTS : DUSD_ADMIN_ACCOUNTS)
            .setIsEnabled(() -> Nxt.getBlockchain().getHeight() >= Constants.UPGRADE_TO_2_3_4_BLOCK)
            .build();

    public static final ChildChain DEUR = new ChildChainBuilder(12, "DEUR")
            .setTotalAmount(10_000_000_000_000000L)
            .setPermissionPolicy(CHILD_CHAIN)
            .setMasterAdminAccounts(Constants.isTestnet ? DASH_TESTNET_ADMIN_ACCOUNTS : DEUR_ADMIN_ACCOUNTS)
            .setIsEnabled(() -> Nxt.getBlockchain().getHeight() >= Constants.UPGRADE_TO_2_3_4_BLOCK)
            .build();

    public static final ChildChain DCAD = new ChildChainBuilder(13, "DCAD")
            .setTotalAmount(10_000_000_000_000000L)
            .setPermissionPolicy(CHILD_CHAIN)
            .setMasterAdminAccounts(Constants.isTestnet ? DASH_TESTNET_ADMIN_ACCOUNTS : DCAD_ADMIN_ACCOUNTS)
            .setIsEnabled(() -> Nxt.getBlockchain().getHeight() >= Constants.UPGRADE_TO_2_3_4_BLOCK)
            .build();

    public static final ChildChain DRMB = new ChildChainBuilder(14, "DRMB")
            .setTotalAmount(10_000_000_000_000000L)
            .setPermissionPolicy(CHILD_CHAIN)
            .setMasterAdminAccounts(Constants.isTestnet ? DASH_TESTNET_ADMIN_ACCOUNTS : DRMB_ADMIN_ACCOUNTS)
            .setIsEnabled(() -> Nxt.getBlockchain().getHeight() >= Constants.UPGRADE_TO_2_3_4_BLOCK)
            .build();

    public static final ChildChain DSGD = new ChildChainBuilder(15, "DSGD")
            .setTotalAmount(10_000_000_000_000000L)
            .setPermissionPolicy(CHILD_CHAIN)
            .setMasterAdminAccounts(Constants.isTestnet ? DASH_TESTNET_ADMIN_ACCOUNTS : DSGD_ADMIN_ACCOUNTS)
            .setIsEnabled(() -> Nxt.getBlockchain().getHeight() >= Constants.UPGRADE_TO_2_3_4_BLOCK)
            .build();

    private static final Long[] ASIAN_TESTNET_ADMIN_ACCOUNTS = Stream.concat(Arrays.stream(testnetAdminAccounts),
            Stream.of("COAL-ML28-G8YP-5VA7-FK2G2", "COAL-8PNJ-8EZ8-MGM7-25N6W").map(Convert::parseAccountId)).toArray(Long[]::new);

    private static final Long[] ASIAN_ADMIN_ACCOUNTS = new Long[]{
            Convert.parseAccountId("COAL-BDUS-BZRA-3THN-G3V3A"),
            Convert.parseAccountId("COAL-4LZT-MTJW-J9VP-AVLJW"),
            Convert.parseAccountId("COAL-KA9K-NEA6-7MGS-8ZHLJ"),
            Convert.parseAccountId("COAL-TWDT-2GRR-XJNV-8HENT"),
            Convert.parseAccountId("COAL-J3QR-2R4M-ZEDF-AVJHW"),
    };

    public static final ChildChain ASIAN = new ChildChainBuilder(16, "ASIAN")
            .setTotalAmount(1_000_000_000_000_000000L)
            .setPermissionPolicy(CHILD_CHAIN)
            .setMasterAdminAccounts(Constants.isTestnet ? ASIAN_TESTNET_ADMIN_ACCOUNTS : ASIAN_ADMIN_ACCOUNTS)
            .setIsEnabled(() -> Nxt.getBlockchain().getHeight() >= Constants.ASIAN_BLOCK)
            .build();

    // for the unit tests only
    public static final ChildChain IGNIS = UST;
    public static final ChildChain AEUR = SGT;
    public static final ChildChain BITSWIFT = MYR;
    public static final ChildChain MPG = ASEAN;

    public static ChildChain getChildChain(String name) {
        return childChains.get(name);
    }

    public static ChildChain getChildChain(int id) {
        return childChainsById.get(id);
    }

    public static Collection<ChildChain> getAll() {
        return allChildChains;
    }

    public static void init() {
        ChildChainLoader.init();
    }

    public final long SHUFFLING_DEPOSIT_NQT;
    private final Supplier<PermissionPolicy> permissionPolicy;
    private final List<Long> masterAdminAccounts;
    private final AliasHome aliasHome;
    private final AssetDividendHome assetDividendHome;
    private final CurrencyFounderHome currencyFounderHome;
    private final DigitalGoodsHome digitalGoodsHome;
    private final ExchangeHome exchangeHome;
    private final ExchangeOfferHome exchangeOfferHome;
    private final ExchangeRequestHome exchangeRequestHome;
    private final OrderHome orderHome;
    private final PhasingPollHome phasingPollHome;
    private final PhasingVoteHome phasingVoteHome;
    private final PollHome pollHome;
    private final ShufflingHome shufflingHome;
    private final ShufflingParticipantHome shufflingParticipantHome;
    private final TaggedDataHome taggedDataHome;
    private final TradeHome tradeHome;
    private final VoteHome voteHome;
    private final Set<TransactionType> disabledTransactionTypes;
    private final BooleanSupplier isEnabled;

    private ChildChain(int id, String name, int decimals, long totalAmount, long shufflingDepositNQT, Set<TransactionType> disabledTransactionTypes,
                       EnumSet<APIEnum> disabledAPIs, EnumSet<APITag> disabledAPITags, BooleanSupplier isEnabled, PermissionPolicyType policyType, PermissionPolicyType nextPolicyType, int policyChangeHeight,
                       List<Long> masterAdminAccounts) {
        super(id, name, decimals, totalAmount, disabledAPIs, disabledAPITags);
        this.SHUFFLING_DEPOSIT_NQT = shufflingDepositNQT;
        final PermissionPolicy policy = policyType.create(this);
        if (nextPolicyType == null) {
            this.permissionPolicy = () -> policy;
        } else {
            final PermissionPolicy nextPolicy = nextPolicyType.create(this);
            this.permissionPolicy = () -> {
                if (Nxt.getBlockchain().getHeight() >= policyChangeHeight || Nxt.getBlockchain().getHeight() == 0 && nextPolicyType == PermissionPolicyType.CHILD_CHAIN) {
                    return nextPolicy;
                }
                return policy;
            };
        }
        this.aliasHome = AliasHome.forChain(this);
        this.assetDividendHome = AssetDividendHome.forChain(this);
        this.currencyFounderHome = CurrencyFounderHome.forChain(this);
        this.digitalGoodsHome = DigitalGoodsHome.forChain(this);
        this.exchangeHome = ExchangeHome.forChain(this);
        this.exchangeOfferHome = ExchangeOfferHome.forChain(this);
        this.exchangeRequestHome = ExchangeRequestHome.forChain(this);
        this.tradeHome = TradeHome.forChain(this);
        this.orderHome = OrderHome.forChain(this);
        this.phasingVoteHome = PhasingVoteHome.forChain(this);
        this.phasingPollHome = PhasingPollHome.forChain(this);
        this.pollHome = PollHome.forChain(this);
        this.shufflingHome = ShufflingHome.forChain(this);
        this.shufflingParticipantHome = ShufflingParticipantHome.forChain(this);
        this.taggedDataHome = TaggedDataHome.forChain(this);
        this.voteHome = VoteHome.forChain(this);
        this.disabledTransactionTypes = Collections.unmodifiableSet(disabledTransactionTypes);
        childChains.put(name, this);
        childChainsById.put(id, this);
        this.isEnabled = isEnabled;
        this.masterAdminAccounts = Collections.unmodifiableList(masterAdminAccounts);
    }

    public AliasHome getAliasHome() {
        return aliasHome;
    }

    public AssetDividendHome getAssetDividendHome() {
        return assetDividendHome;
    }

    public CurrencyFounderHome getCurrencyFounderHome() {
        return currencyFounderHome;
    }

    public DigitalGoodsHome getDigitalGoodsHome() {
        return digitalGoodsHome;
    }

    public ExchangeHome getExchangeHome() {
        return exchangeHome;
    }

    public ExchangeOfferHome getExchangeOfferHome() {
        return exchangeOfferHome;
    }

    public ExchangeRequestHome getExchangeRequestHome() {
        return exchangeRequestHome;
    }

    public OrderHome getOrderHome() {
        return orderHome;
    }

    public PhasingPollHome getPhasingPollHome() {
        return phasingPollHome;
    }

    public PhasingVoteHome getPhasingVoteHome() {
        return phasingVoteHome;
    }

    public PollHome getPollHome() {
        return pollHome;
    }

    public ShufflingHome getShufflingHome() {
        return shufflingHome;
    }

    public ShufflingParticipantHome getShufflingParticipantHome() {
        return shufflingParticipantHome;
    }

    public TaggedDataHome getTaggedDataHome() {
        return taggedDataHome;
    }

    public TradeHome getTradeHome() {
        return tradeHome;
    }

    public VoteHome getVoteHome() {
        return voteHome;
    }

    @Override
    public boolean isAllowed(TransactionType transactionType) {
        return transactionType.getType() >= 0 && !disabledTransactionTypes.contains(transactionType) && (this == IGNIS || !transactionType.isGlobal());
    }

    @Override
    public Set<TransactionType> getDisabledTransactionTypes() {
        return disabledTransactionTypes;
    }

    @Override
    public Set<APITag> getDisabledAPITags() {
        Set<APITag> permissionPolicyDisabledAPITags = getPermissionPolicy().getDisabledAPITags();
        if (permissionPolicyDisabledAPITags.isEmpty()) {
            return super.getDisabledAPITags();
        }
        Set<APITag> set = new HashSet<>(super.getDisabledAPITags());
        set.addAll(permissionPolicyDisabledAPITags);
        return set;
    }

    public boolean isEnabled() {
        return isEnabled.getAsBoolean();
    }

    @Override
    public ChildTransactionImpl.BuilderImpl newTransactionBuilder(byte[] senderPublicKey, long amount, long fee, short deadline, Attachment attachment) {
        return ChildTransactionImpl.newTransactionBuilder(this.getId(), (byte) 1, senderPublicKey, amount, fee, deadline, (Attachment.AbstractAttachment) attachment);
    }

    @Override
    ChildTransactionImpl.BuilderImpl newTransactionBuilder(byte version, byte[] senderPublicKey, long amount, long fee, short deadline,
                                                           List<Appendix.AbstractAppendix> appendages, JSONObject transactionData) {
        return ChildTransactionImpl.newTransactionBuilder(this.getId(), version, senderPublicKey, amount, fee, deadline,
                appendages, transactionData);
    }

    @Override
    ChildTransactionImpl.BuilderImpl newTransactionBuilder(byte version, byte[] senderPublicKey, long amount, long fee, short deadline,
                                                           List<Appendix.AbstractAppendix> appendages, ByteBuffer buffer) {
        return ChildTransactionImpl.newTransactionBuilder(this.getId(), version, senderPublicKey, amount, fee, deadline,
                appendages, buffer);
    }

    @Override
    ChildTransactionImpl.BuilderImpl newTransactionBuilder(byte version, long amount, long fee, short deadline,
                                                           List<Appendix.AbstractAppendix> appendages, ResultSet rs) {
        return ChildTransactionImpl.newTransactionBuilder(this.getId(), version, amount, fee, deadline, appendages, rs);
    }

    @Override
    UnconfirmedTransaction newUnconfirmedTransaction(ResultSet rs) throws SQLException, NxtException.NotValidException {
        return new UnconfirmedChildTransaction(rs);
    }

    public PermissionChecker getPermissionChecker() {
        return getPermissionPolicy().getPermissionChecker();
    }

    public PermissionWriter getPermissionWriter(long granterId) {
        return getPermissionPolicy().getPermissionWriter(granterId);
    }

    public PermissionReader getPermissionReader() {
        return getPermissionPolicy().getPermissionReader();
    }

    public PermissionPolicy getPermissionPolicy() {
        return permissionPolicy.get();
    }

    public List<Long> getMasterAdminAccounts() {
        return masterAdminAccounts;
    }

    private static class ChildChainBuilder {
        private final int id;
        private final String name;
        private long totalAmount;
        private long shufflingDepositNQT;
        private int decimals = 6;
        private Set<TransactionType> disabledTransactionTypes =
                linkedHashSet(
                        AssetExchangeTransactionType.ASSET_ISSUANCE,
                        AssetExchangeTransactionType.ASSET_DELETE, AssetExchangeTransactionType.ASSET_TRANSFER,
                        AssetExchangeTransactionType.ASK_ORDER_PLACEMENT, AssetExchangeTransactionType.ASK_ORDER_CANCELLATION,
                        AssetExchangeTransactionType.BID_ORDER_PLACEMENT, AssetExchangeTransactionType.BID_ORDER_CANCELLATION,
                        AssetExchangeTransactionType.DIVIDEND_PAYMENT,
                        AssetExchangeTransactionType.ASSET_PROPERTY_SET, AssetExchangeTransactionType.ASSET_PROPERTY_DELETE,
                        AssetExchangeTransactionType.SET_PHASING_CONTROL,
                        VotingTransactionType.POLL_CREATION,
                        ShufflingTransactionType.SHUFFLING_CREATION,
                        MonetarySystemTransactionType.CURRENCY_ISSUANCE,
                        MonetarySystemTransactionType.CURRENCY_DELETION, MonetarySystemTransactionType.CURRENCY_MINTING,
                        MonetarySystemTransactionType.CURRENCY_TRANSFER, MonetarySystemTransactionType.PUBLISH_EXCHANGE_OFFER,
                        MonetarySystemTransactionType.EXCHANGE_BUY, MonetarySystemTransactionType.EXCHANGE_SELL,
                        MonetarySystemTransactionType.RESERVE_INCREASE, MonetarySystemTransactionType.RESERVE_CLAIM,
                        AliasTransactionType.ALIAS_ASSIGNMENT,
                        LightContractTransactionType.CONTRACT_REFERENCE_SET, LightContractTransactionType.CONTRACT_REFERENCE_DELETE);
        private EnumSet<APIEnum> disabledAPIs = EnumSet.noneOf(APIEnum.class);
        private EnumSet<APITag> disabledAPITags = EnumSet.of(APITag.AE, APITag.VS, APITag.SHUFFLING, APITag.MS, APITag.ALIASES);
        private BooleanSupplier isEnabled = () -> true;
        private PermissionPolicyType policyType = PermissionPolicyType.NONE;
        private PermissionPolicyType nextPolicyType = null;
        private int policyChangeHeight = 0;
        private List<Long> masterAdminAccounts = Collections.emptyList();

        private ChildChainBuilder(int id, String name) {
            this.id = id;
            this.name = name;
        }

        private ChildChainBuilder setDecimals(int decimals) {
            this.decimals = decimals;
            return this;
        }

        private ChildChainBuilder setDisabledTransactionTypes(TransactionType... disabledTransactionTypes) {
            this.disabledTransactionTypes = linkedHashSet(disabledTransactionTypes);
            return this;
        }

        private ChildChainBuilder setDisabledAPIs(APIEnum... disabledAPIs) {
            this.disabledAPIs = enumSet(disabledAPIs);
            return this;
        }

        private ChildChainBuilder setDisabledAPITags(APITag... disabledAPITags) {
            this.disabledAPITags = enumSet(disabledAPITags);
            return this;
        }

        private ChildChainBuilder setTotalAmount(long totalAmount) {
            this.totalAmount = totalAmount;
            return this;
        }

        private ChildChainBuilder setShufflingDepositNQT(long shufflingDepositNQT) {
            this.shufflingDepositNQT = shufflingDepositNQT;
            return this;
        }

        private ChildChainBuilder setIsEnabled(BooleanSupplier isEnabled) {
            this.isEnabled = isEnabled;
            return this;
        }

        private ChildChainBuilder setPermissionPolicy(PermissionPolicyType policyType) {
            this.policyType = policyType;
            return this;
        }

        private ChildChainBuilder setNextPermissionPolicy(PermissionPolicyType policyType, int policyChangeHeight) {
            this.nextPolicyType = policyType;
            this.policyChangeHeight = policyChangeHeight;
            return this;
        }

        private ChildChainBuilder setMasterAdminAccounts(Long... masterAdminAccounts) {
            this.masterAdminAccounts = Arrays.asList(masterAdminAccounts);
            return this;
        }

        private ChildChain build() {
            return new ChildChain(id, name, decimals, totalAmount, shufflingDepositNQT, disabledTransactionTypes,
                    disabledAPIs, disabledAPITags, isEnabled, policyType, nextPolicyType, policyChangeHeight, masterAdminAccounts);
        }

        @SafeVarargs
        private static <T extends Enum<T>> EnumSet<T> enumSet(T... elements) {
            return EnumSet.copyOf(Arrays.asList(elements));
        }

        private static Set<TransactionType> linkedHashSet(TransactionType... types) {
            return new LinkedHashSet<>(Arrays.asList(types));
        }
    }
}

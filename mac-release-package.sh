#!/bin/bash
VERSION=$1
if [ -x ${VERSION} ];
then
	echo VERSION not defined
	exit 1
fi
APPLICATION="coalculus"
PACKAGE=${APPLICATION}-client-${VERSION}
echo PACKAGE="${PACKAGE}"
CHANGELOG=${APPLICATION}-client-${VERSION}.changelog.txt
OBFUSCATE=$2

MACVERSION=$3
if [ -x ${MACVERSION} ];
then
MACVERSION=${VERSION}
fi
echo MACVERSION="${MACVERSION}"

# Force Java 8 even if a newer one is the default. Or else the installer will be broken is
export JAVA_HOME=`/usr/libexec/java_home -v 1.8`

export JAVA_TOOL_OPTIONS="$JAVA_TOOL_OPTIONS -Dfile.encoding=UTF8"

FILES="changelogs conf html lib testlib resource contrib"
FILES="${FILES} ardor.exe ardorservice.exe"
FILES="${FILES} 3RD-PARTY-LICENSES.txt LICENSE.txt"
FILES="${FILES} DEVELOPERS-GUIDE.md OPERATORS-GUIDE.md README.md README.txt USERS-GUIDE.md"
FILES="${FILES} mint.bat mint.sh run.bat run.sh run-desktop.sh start.sh stop.sh compact.sh compact.bat sign.sh sign.bat"
FILES="${FILES} passphraseRecovery.sh passphraseRecovery.bat contractManager.sh contractManager.bat pem.to.pkcs12.keystore.certbot.hook.sh"
FILES="${FILES} run-taxreport.sh run-taxreport.bat"
FILES="${FILES} ardor.policy ardordesktop.policy contractManager.policy Ardor_Wallet.url"

echo compile
./compile.sh
rm -rf html/doc/*
rm -rf ${APPLICATION}
rm -rf ${PACKAGE}.jar
rm -rf ${PACKAGE}.exe
rm -rf ${PACKAGE}.zip
mkdir -p ${APPLICATION}/
mkdir -p ${APPLICATION}/logs
mkdir -p ${APPLICATION}/addons/src

if [ "${OBFUSCATE}" = "obfuscate" ]; 
then
echo obfuscate
~/proguard/proguard5.3.3/bin/proguard.sh @nxt.pro
mv ../nxt.map ../nxt.map.${VERSION}
else
FILES="${FILES} classes src test addons JPL-Ardor.pdf"
FILES="${FILES} compile.sh javadoc.sh jar.sh package.sh generateAPICalls.sh"
echo javadoc
./javadoc.sh
fi
echo copy resources
cp installer/lib/JavaExe.exe ardor.exe
cp installer/lib/JavaExe.exe ardorservice.exe
cp -a ${FILES} ${APPLICATION}
cp -a logs/placeholder.txt ${APPLICATION}/logs
echo gzip
for f in `find ${APPLICATION}/html -name *.gz`
do
	rm -f "$f"
done
for f in `find ${APPLICATION}/html -name *.html -o -name *.js -o -name *.css -o -name *.json  -o -name *.ttf -o -name *.svg -o -name *.otf`
do
	gzip -9c "$f" > "$f".gz
done
cd ${APPLICATION}
echo generate jar files
../jar.sh
../jar-tests.sh
echo package installer Jar
../installer/build-installer.sh ../${PACKAGE}
cd -
rm -rf ${APPLICATION}

echo bundle a dmg file	
$JAVA_HOME/bin/javapackager -deploy -outdir . -outfile ${APPLICATION}-client -name ${APPLICATION}-installer -width 34 -height 43 -native dmg -srcfiles ${PACKAGE}.jar -appclass com.izforge.izpack.installer.bootstrap.Installer -v -Bmac.category=Business -Bmac.CFBundleIdentifier=org.ardor.client.installer -Bmac.CFBundleName=Ardor-Installer -Bmac.CFBundleVersion=${MACVERSION} -BappVersion=${MACVERSION} -Bicon=installer/AppIcon.icns > installer/javapackager.log 2>&1

mv bundles/${APPLICATION}-installer-${MACVERSION}.dmg bundles/${APPLICATION}-installer-${VERSION}.dmg
